package ru.payda.backend.service;

import org.springframework.web.multipart.MultipartFile;
import ru.payda.backend.service.dto.FundDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ru.payda.backend.domain.Fund}.
 */
public interface FundService {

    /**
     * Save a fund.
     *
     * @param fundDTO the entity to save.
     * @return the persisted entity.
     */
    FundDTO save(FundDTO fundDTO);

    /**
     * Get all the funds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FundDTO> findAll(Pageable pageable);

    /**
     * Get all the funds with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<FundDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" fund.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FundDTO> findOne(Long id);

    /**
     * Delete the "id" fund.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    FundDTO attachLogo(Long fundId, MultipartFile file);

    FundDTO attachBackground(Long fundId, MultipartFile file);
}
