package ru.payda.backend.service;

import ru.payda.backend.service.dto.DonationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ru.payda.backend.domain.Donation}.
 */
public interface DonationService {

    /**
     * Save a donation.
     *
     * @param donationDTO the entity to save.
     * @return the persisted entity.
     */
    DonationDTO save(DonationDTO donationDTO);

    /**
     * Get all the donations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DonationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" donation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DonationDTO> findOne(Long id);

    /**
     * Delete the "id" donation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
