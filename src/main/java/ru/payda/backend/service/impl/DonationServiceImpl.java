package ru.payda.backend.service.impl;

import ru.payda.backend.service.DonationService;
import ru.payda.backend.domain.Donation;
import ru.payda.backend.repository.DonationRepository;
import ru.payda.backend.service.dto.DonationDTO;
import ru.payda.backend.service.mapper.DonationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Donation}.
 */
@Service
@Transactional
public class DonationServiceImpl implements DonationService {

    private final Logger log = LoggerFactory.getLogger(DonationServiceImpl.class);

    private final DonationRepository donationRepository;

    private final DonationMapper donationMapper;

    public DonationServiceImpl(DonationRepository donationRepository, DonationMapper donationMapper) {
        this.donationRepository = donationRepository;
        this.donationMapper = donationMapper;
    }

    /**
     * Save a donation.
     *
     * @param donationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DonationDTO save(DonationDTO donationDTO) {
        log.debug("Request to save Donation : {}", donationDTO);
        Donation donation = donationMapper.toEntity(donationDTO);
        donation = donationRepository.save(donation);
        return donationMapper.toDto(donation);
    }

    /**
     * Get all the donations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DonationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Donations");
        return donationRepository.findAll(pageable)
            .map(donationMapper::toDto);
    }


    /**
     * Get one donation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DonationDTO> findOne(Long id) {
        log.debug("Request to get Donation : {}", id);
        return donationRepository.findById(id)
            .map(donationMapper::toDto);
    }

    /**
     * Delete the donation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Donation : {}", id);

        donationRepository.deleteById(id);
    }
}
