package ru.payda.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.payda.backend.domain.Fee;
import ru.payda.backend.domain.FileInfo;
import ru.payda.backend.repository.FeeRepository;
import ru.payda.backend.repository.FileRepository;
import ru.payda.backend.service.FeeService;
import ru.payda.backend.service.FileStorageService;
import ru.payda.backend.service.dto.FeeDTO;
import ru.payda.backend.service.mapper.FeeMapper;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Fee}.
 */
@Service
@Transactional
public class FeeServiceImpl implements FeeService {

    private final Logger log = LoggerFactory.getLogger(FeeServiceImpl.class);

    private final FeeRepository feeRepository;

    private final FeeMapper feeMapper;

    private final FileRepository fileRepository;

    private final FileStorageService fileStorageService;

    public FeeServiceImpl(FeeRepository feeRepository, FeeMapper feeMapper, FileRepository fileRepository, FileStorageService fileStorageService) {
        this.feeRepository = feeRepository;
        this.feeMapper = feeMapper;
        this.fileRepository = fileRepository;
        this.fileStorageService = fileStorageService;
    }

    /**
     * Save a fee.
     *
     * @param feeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FeeDTO save(FeeDTO feeDTO) {
        log.debug("Request to save Fee : {}", feeDTO);
        Fee fee = feeMapper.toEntity(feeDTO);
        fee = feeRepository.save(fee);
        return feeMapper.toDto(fee);
    }

    @Override
    public FeeDTO attachFiles(Long feeId, MultipartFile[] files) {
        Fee fee = this.feeRepository.findById(feeId).orElseThrow(() -> new EntityNotFoundException("fee: " + feeId));

        List<FileInfo> fileInfos = Arrays.stream(files).map(f -> {
            FileInfo fileInfo = new FileInfo();
            fileInfo.setName(UUID.randomUUID().toString());
            fileInfo.setContentType(f.getContentType());
            fileInfo.setSize(f.getSize());
            try {
                this.fileStorageService.save(fileInfo.getName(), f.getInputStream());
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
            return fileInfo;
        }).collect(Collectors.toList());

        fee.addFiles(fileInfos);

        fee = this.feeRepository.save(fee);

        return this.feeMapper.toDto(fee);

    }

    /**
     * Get all the fees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FeeDTO> findAll(FeeDTO exampleDTO, Pageable pageable) {
        log.debug("Request to get all Fees");
        Fee exampleEntity = feeMapper.toEntity(exampleDTO);
        Example<Fee> example = Example.of(exampleEntity, ExampleMatcher.matchingAll().withIgnoreNullValues());
        return feeRepository
            .findAll(example, pageable)
            .map(feeMapper::toDto);
    }


    /**
     * Get one fee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FeeDTO> findOne(Long id) {
        log.debug("Request to get Fee : {}", id);
        return feeRepository.findById(id)
            .map(feeMapper::toDto);
    }

    /**
     * Delete the fee by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Fee : {}", id);

        feeRepository.deleteById(id);
    }
}
