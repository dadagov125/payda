package ru.payda.backend.service.impl;

import ru.payda.backend.service.PaymentSystemService;
import ru.payda.backend.domain.PaymentSystem;
import ru.payda.backend.repository.PaymentSystemRepository;
import ru.payda.backend.service.dto.PaymentSystemDTO;
import ru.payda.backend.service.mapper.PaymentSystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link PaymentSystem}.
 */
@Service
@Transactional
public class PaymentSystemServiceImpl implements PaymentSystemService {

    private final Logger log = LoggerFactory.getLogger(PaymentSystemServiceImpl.class);

    private final PaymentSystemRepository paymentSystemRepository;

    private final PaymentSystemMapper paymentSystemMapper;

    public PaymentSystemServiceImpl(PaymentSystemRepository paymentSystemRepository, PaymentSystemMapper paymentSystemMapper) {
        this.paymentSystemRepository = paymentSystemRepository;
        this.paymentSystemMapper = paymentSystemMapper;
    }

    /**
     * Save a paymentSystem.
     *
     * @param paymentSystemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PaymentSystemDTO save(PaymentSystemDTO paymentSystemDTO) {
        log.debug("Request to save PaymentSystem : {}", paymentSystemDTO);
        PaymentSystem paymentSystem = paymentSystemMapper.toEntity(paymentSystemDTO);
        paymentSystem = paymentSystemRepository.save(paymentSystem);
        return paymentSystemMapper.toDto(paymentSystem);
    }

    /**
     * Get all the paymentSystems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PaymentSystemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PaymentSystems");
        return paymentSystemRepository.findAll(pageable)
            .map(paymentSystemMapper::toDto);
    }



    /**
     *  Get all the paymentSystems where Fund is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<PaymentSystemDTO> findAllWhereFundIsNull() {
        log.debug("Request to get all paymentSystems where Fund is null");
        return StreamSupport
            .stream(paymentSystemRepository.findAll().spliterator(), false)
            .filter(paymentSystem -> paymentSystem.getFund() == null)
            .map(paymentSystemMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one paymentSystem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentSystemDTO> findOne(Long id) {
        log.debug("Request to get PaymentSystem : {}", id);
        return paymentSystemRepository.findById(id)
            .map(paymentSystemMapper::toDto);
    }

    /**
     * Delete the paymentSystem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PaymentSystem : {}", id);

        paymentSystemRepository.deleteById(id);
    }
}
