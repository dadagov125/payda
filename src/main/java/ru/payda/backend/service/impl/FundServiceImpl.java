package ru.payda.backend.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.payda.backend.domain.FileInfo;
import ru.payda.backend.domain.Fund;
import ru.payda.backend.repository.FileRepository;
import ru.payda.backend.repository.FundRepository;
import ru.payda.backend.service.FileStorageService;
import ru.payda.backend.service.FundService;
import ru.payda.backend.service.dto.FundDTO;
import ru.payda.backend.service.mapper.FundMapper;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

/**
 * Service Implementation for managing {@link Fund}.
 */
@Service
@Transactional
public class FundServiceImpl implements FundService {

    private final Logger log = LoggerFactory.getLogger(FundServiceImpl.class);

    private final FundRepository fundRepository;

    private final FundMapper fundMapper;

    private final FileStorageService fileStorageService;

    private FileRepository fileRepository;

    public FundServiceImpl(FundRepository fundRepository, FundMapper fundMapper, FileStorageService fileStorageService, FileRepository fileRepository) {
        this.fundRepository = fundRepository;
        this.fundMapper = fundMapper;
        this.fileStorageService = fileStorageService;
        this.fileRepository = fileRepository;
    }

    /**
     * Save a fund.
     *
     * @param fundDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public FundDTO save(FundDTO fundDTO) {
        log.debug("Request to save Fund : {}", fundDTO);
        Fund fund = fundMapper.toEntity(fundDTO);
        fund = fundRepository.save(fund);
        return fundMapper.toDto(fund);
    }

    /**
     * Get all the funds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FundDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Funds");
        return fundRepository.findAll(pageable)
            .map(fundMapper::toDto);
    }


    /**
     * Get all the funds with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<FundDTO> findAllWithEagerRelationships(Pageable pageable) {
        return fundRepository.findAllWithEagerRelationships(pageable).map(fundMapper::toDto);
    }

    /**
     * Get one fund by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FundDTO> findOne(Long id) {
        log.debug("Request to get Fund : {}", id);
        return fundRepository.findOneWithEagerRelationships(id)
            .map(fundMapper::toDto);
    }

    /**
     * Delete the fund by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Fund : {}", id);

        fundRepository.deleteById(id);
    }

    @Override
    public FundDTO attachLogo(Long fundId, MultipartFile file) {

        Fund fund = this.fundRepository.findById(fundId).orElseThrow(() -> new EntityNotFoundException("fund: " + fundId));

        if (fund.getLogo() != null) {

            this.fileStorageService.delete(fund.getLogo().getName());
            this.fileRepository.deleteById(fund.getLogo().getId());
            fund.setLogo(null);
//            fund = this.fundRepository.save(fund);
        }
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(UUID.randomUUID().toString());
        fileInfo.setContentType(file.getContentType());
        fileInfo.setSize(file.getSize());

        try {
            this.fileStorageService.save(fileInfo.getName(), file.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        fund.setLogo(fileInfo);
        return fundMapper.toDto(this.fundRepository.save(fund));
    }

    @Override
    public FundDTO attachBackground(Long fundId, MultipartFile file) {
        Fund fund = this.fundRepository.findById(fundId).orElseThrow(() -> new EntityNotFoundException("fund: " + fundId));

        if (fund.getBackground() != null) {

            this.fileStorageService.delete(fund.getBackground().getName());
            this.fileRepository.deleteById(fund.getBackground().getId());
            fund.setBackground(null);
//            fund = this.fundRepository.save(fund);
        }
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(UUID.randomUUID().toString());
        fileInfo.setContentType(file.getContentType());
        fileInfo.setSize(file.getSize());

        try {
            this.fileStorageService.save(fileInfo.getName(), file.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        fund.setBackground(fileInfo);
        return fundMapper.toDto(this.fundRepository.save(fund));

    }
}
