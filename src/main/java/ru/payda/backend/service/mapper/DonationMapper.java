package ru.payda.backend.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.payda.backend.domain.Donation;
import ru.payda.backend.service.dto.DonationDTO;

/**
 * Mapper for the entity {@link Donation} and its DTO {@link DonationDTO}.
 */
@Mapper(componentModel = "spring", uses = {FeeMapper.class})
public interface DonationMapper extends EntityMapper<DonationDTO, Donation> {

    @Mapping(source = "fee.id", target = "feeId")
    DonationDTO toDto(Donation donation);


    default Donation toEntity(DonationDTO donationDTO) {
        return null;
    }

    default Donation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Donation donation = new Donation();
        donation.setId(id);
        return donation;
    }
}
