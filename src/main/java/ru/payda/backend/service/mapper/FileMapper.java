package ru.payda.backend.service.mapper;


import org.mapstruct.Mapper;
import ru.payda.backend.domain.FileInfo;
import ru.payda.backend.service.dto.FileDTO;

import java.util.List;

/**
 * Mapper for the entity {@link FileInfo} and its DTO {@link FileDTO}.
 */
@Mapper(componentModel = "spring")
public interface FileMapper extends EntityMapper<FileDTO, FileInfo> {


    default FileInfo toEntity(FileDTO dto) {
        return null;
    }

    FileDTO toDto(FileInfo fileInfo);


    List<FileInfo> toEntity(List<FileDTO> dtoList);

    List<FileDTO> toDto(List<FileInfo> entityList);

}
