package ru.payda.backend.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.payda.backend.domain.Fee;
import ru.payda.backend.service.dto.FeeDTO;

import java.math.BigDecimal;

/**
 * Mapper for the entity {@link Fee} and its DTO {@link FeeDTO}.
 */
@Mapper(componentModel = "spring", imports = {BigDecimal.class}, uses = {FundMapper.class, FileMapper.class})
public interface FeeMapper extends EntityMapper<FeeDTO, Fee> {

    @Mapping(target = "donations", expression = "java(fee.getDonations().stream().map(d->d.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add))")
    @Mapping(target = "fund.users", ignore = true)
    @Mapping(target = "fund.logo", ignore = true)
    @Mapping(target = "fund.background", ignore = true)
    @Mapping(target = "fund.paymentSystemId", ignore = true)
    FeeDTO toDto(Fee fee);


    @Mapping(target = "donations", ignore = true)
    @Mapping(target = "removeDonation", ignore = true)
    @Mapping(target = "fund.removeFees", ignore = true)
    @Mapping(target = "fund.removeUser", ignore = true)
    @Mapping(target = "fund.users", ignore = true)
    @Mapping(target = "fund.logo", ignore = true)
    @Mapping(target = "fund.background", ignore = true)
    @Mapping(target = "fund.paymentSystem", ignore = true)
    @Mapping(target = "fund.fees", ignore = true)
    @Mapping(target = "files", ignore = true)
    @Mapping(source = "fund.id", target = "fund.id")
    @Mapping(target = "removeFile", ignore = true)
    Fee toEntity(FeeDTO feeDTO);

    default Fee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Fee fee = new Fee();
        fee.setId(id);
        return fee;
    }
}
