package ru.payda.backend.service.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.payda.backend.domain.Fund;
import ru.payda.backend.service.dto.FundDTO;

/**
 * Mapper for the entity {@link Fund} and its DTO {@link FundDTO}.
 */
@Mapper(componentModel = "spring", uses = {PaymentSystemMapper.class, UserMapper.class, FeeMapper.class, FileMapper.class})
public interface FundMapper extends EntityMapper<FundDTO, Fund> {

    @Mapping(source = "paymentSystem.id", target = "paymentSystemId")
    FundDTO toDto(Fund fund);

    @Mapping(source = "paymentSystemId", target = "paymentSystem")
    @Mapping(target = "fees", ignore = true)
    @Mapping(target = "removeFees", ignore = true)
    @Mapping(target = "removeUser", ignore = true)
    @Mapping(target = "logo", ignore = true)
    @Mapping(target = "background", ignore = true)
    Fund toEntity(FundDTO fundDTO);

    default Fund fromId(Long id) {
        if (id == null) {
            return null;
        }
        Fund fund = new Fund();
        fund.setId(id);
        return fund;
    }
}
