package ru.payda.backend.service.mapper;


import org.mapstruct.Mapper;
import ru.payda.backend.domain.PaymentSystem;
import ru.payda.backend.service.dto.PaymentSystemDTO;

/**
 * Mapper for the entity {@link PaymentSystem} and its DTO {@link PaymentSystemDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PaymentSystemMapper extends EntityMapper<PaymentSystemDTO, PaymentSystem> {


    //    @Mapping(target = "fund", ignore = true)
//    @Mapping(target = "settings", ignore = true)
    default PaymentSystem toEntity(PaymentSystemDTO paymentSystemDTO) {
        return null;
    }

    default PaymentSystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        PaymentSystem paymentSystem = new PaymentSystem();
        paymentSystem.setId(id);
        return paymentSystem;
    }
}
