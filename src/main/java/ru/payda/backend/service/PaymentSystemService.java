package ru.payda.backend.service;

import ru.payda.backend.service.dto.PaymentSystemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ru.payda.backend.domain.PaymentSystem}.
 */
public interface PaymentSystemService {

    /**
     * Save a paymentSystem.
     *
     * @param paymentSystemDTO the entity to save.
     * @return the persisted entity.
     */
    PaymentSystemDTO save(PaymentSystemDTO paymentSystemDTO);

    /**
     * Get all the paymentSystems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PaymentSystemDTO> findAll(Pageable pageable);
    /**
     * Get all the PaymentSystemDTO where Fund is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<PaymentSystemDTO> findAllWhereFundIsNull();


    /**
     * Get the "id" paymentSystem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PaymentSystemDTO> findOne(Long id);

    /**
     * Delete the "id" paymentSystem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
