package ru.payda.backend.service;

import org.springframework.web.multipart.MultipartFile;
import ru.payda.backend.service.dto.FeeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ru.payda.backend.domain.Fee}.
 */
public interface FeeService {

    /**
     * Save a fee.
     *
     * @param feeDTO the entity to save.
     * @return the persisted entity.
     */
    FeeDTO save(FeeDTO feeDTO);

    FeeDTO attachFiles(Long feeId, MultipartFile[] files);

    /**
     * Get all the fees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FeeDTO> findAll(FeeDTO exampleDTO, Pageable pageable);


    /**
     * Get the "id" fee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FeeDTO> findOne(Long id);

    /**
     * Delete the "id" fee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
