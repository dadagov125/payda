package ru.payda.backend.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.payda.backend.domain.PaymentSystem} entity.
 */
public class PaymentSystemDTO implements Serializable {
    
    private Long id;

    private String name;

    private String settings;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PaymentSystemDTO)) {
            return false;
        }

        return id != null && id.equals(((PaymentSystemDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentSystemDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", settings='" + getSettings() + "'" +
            "}";
    }
}
