package ru.payda.backend.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link ru.payda.backend.domain.Fund} entity.
 */
public class FundDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String region;


    private FileDTO logo;

    private FileDTO background;

    private Long paymentSystemId;

    private Set<UserDTO> users = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }


    public Long getPaymentSystemId() {
        return paymentSystemId;
    }

    public void setPaymentSystemId(Long paymentSystemId) {
        this.paymentSystemId = paymentSystemId;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FundDTO)) {
            return false;
        }

        return id != null && id.equals(((FundDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FundDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", region='" + getRegion() + "'" +
            ", paymentSystemId=" + getPaymentSystemId() +
            ", users='" + getUsers() + "'" +
            "}";
    }

    public FileDTO getLogo() {
        return logo;
    }

    public void setLogo(FileDTO logo) {
        this.logo = logo;
    }

    public FileDTO getBackground() {
        return background;
    }

    public void setBackground(FileDTO background) {
        this.background = background;
    }
}
