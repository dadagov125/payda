package ru.payda.backend.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.payda.backend.domain.FileInfo} entity.
 */
public class FileDTO implements Serializable {

    @NotNull
    private String name;

    @NotNull
    private Long size;

    private String contentType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileDTO)) {
            return false;
        }
        FileDTO other = (FileDTO) o;
        return name.equals(other.name) && size.equals(other.size) && contentType.equals(other.contentType);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FileDTO{" +
            ", name='" + getName() + "'" +
            ", size=" + getSize() +
            ", contentType='" + getContentType() + "'" +
            "}";
    }
}
