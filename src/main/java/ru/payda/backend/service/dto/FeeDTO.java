package ru.payda.backend.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO for the {@link ru.payda.backend.domain.Fee} entity.
 */
public class FeeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    private BigDecimal amount;

    private String region;

    private Boolean active;

    private FundDTO fund;

    private List<FileDTO> files = new ArrayList<>();

    private BigDecimal donations = BigDecimal.valueOf(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public FundDTO getFund() {
        return fund;
    }

    public void setFund(FundDTO fund) {
        this.fund = fund;
    }


    public List<FileDTO> getFiles() {
        return files;
    }

    public FeeDTO addFile(FileDTO file) {
        this.files.add(file);
        return this;
    }


    public void setFiles(List<FileDTO> files) {
        this.files = files;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FeeDTO)) {
            return false;
        }

        return id != null && id.equals(((FeeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FeeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", amount=" + getAmount() +
            ", region='" + getRegion() + "'" +
            ", fundId=" + getFund() +
            "}";
    }

    public BigDecimal getDonations() {
        return donations;
    }

    public void setDonations(BigDecimal donations) {
        this.donations = donations;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
