package ru.payda.backend.service.integration.ykassa.models;

public enum ConfirmationType{
    redirect,
    external,
    qr,
    embeded
}
