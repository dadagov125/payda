package ru.payda.backend.service.integration.ykassa.models;

public class PaymentResponse {

    private Payment payment;

    private PaymentError error;

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public PaymentError getError() {
        return error;
    }

    public void setError(PaymentError error) {
        this.error = error;
    }
}
