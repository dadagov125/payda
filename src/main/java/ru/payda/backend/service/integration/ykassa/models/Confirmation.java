package ru.payda.backend.service.integration.ykassa.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Confirmation {

    /*
    Код сценария подтверждения.
     */
    private ConfirmationType type;
    /*
    embeded
    токен для инициализации платежного виджета Яндекс.Кассы .
     */
    private String confirmation_token;

    /*
    qr
    Данные для генерации QR-кода.
     */
    private String confirmation_data;


    /*
    redirect
    URL, на который необходимо перенаправить пользователя для подтверждения оплаты.
     */
    private String confirmation_url;

    /*
    Язык интерфейса, писем и смс, которые будет видеть или получать пользователь.
    Формат соответствует ISO/IEC 15897. Возможные значения: ru_RU, en_US. Регистр важен.
     */
    private String locale = "ru_RU";


    /*
    redirect
    Запрос на проведение платежа с аутентификацией по 3-D Secure.
    Будет работать, если оплату банковской картой вы по умолчанию принимаете без подтверждения платежа пользователем.
    В остальных случаях аутентификацией по 3-D Secure будет управлять Яндекс.Касса.
    Если хотите принимать платежи без дополнительного подтверждения пользователем, напишите вашему менеджеру Яндекс.Кассы.
     */
    private boolean enforce;

    /*
    redirect
    URL, на который вернется пользователь после подтверждения или отмены платежа на веб-странице.
     */
    private String return_url;

    public ConfirmationType getType() {
        return type;
    }

    public void setType(ConfirmationType type) {
        this.type = type;
    }

    public String getConfirmation_token() {
        return confirmation_token;
    }

    public void setConfirmation_token(String confirmation_token) {
        this.confirmation_token = confirmation_token;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public boolean isEnforce() {
        return enforce;
    }

    public void setEnforce(boolean enforce) {
        this.enforce = enforce;
    }

    public String getReturn_url() {
        return return_url;
    }

    public void setReturn_url(String return_url) {
        this.return_url = return_url;
    }

    public String getConfirmation_data() {
        return confirmation_data;
    }

    public void setConfirmation_data(String confirmation_data) {
        this.confirmation_data = confirmation_data;
    }

    public String getConfirmation_url() {
        return confirmation_url;
    }

    public void setConfirmation_url(String confirmation_url) {
        this.confirmation_url = confirmation_url;
    }
}
