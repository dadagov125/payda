package ru.payda.backend.service.integration.ykassa.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment {

    /*
    Идентификатор платежа в Яндекс.Кассе.
     */
    protected String id;

    /*
    Статус платежа. Возможные значения: pending, waiting_for_capture, succeeded и canceled.
     */
    protected PaymentStatus status;

    /*
    Одноразовый токен для проведения оплаты, сформированный с помощью веб или мобильного SDK .
     */
    protected String payment_token;

    /*
    Сумма платежа.
    Иногда партнеры Яндекс.Кассы берут с пользователя дополнительную комиссию,
    которая не входит в эту сумму.
     */
    protected Amount amount;

    /*
    Сумма платежа, которую получит магазин — значение amount за вычетом комиссии Яндекс.Кассы.
    Если вы партнер  и для аутентификации запросов используете OAuth-токен,
    запросите у магазина право  на получение информации о комиссиях при платежах.
     */
    protected Amount income_amount;

    /*
    Описание транзакции (не более 128 символов), которое вы увидите в личном кабинете Яндекс.Кассы,
    а пользователь — при оплате. Например: «Оплата заказа № 72 для user@yandex.ru».
     */
    protected String description;


    protected Boolean capture;

    /*
    Получатель платежа.
     */
    protected Recipient recipient;


    /*
    Способ оплаты , который был использован для этого платежа.
     */
    protected PaymentMethod payment_method;


    /*
    Время подтверждения платежа. Указывается по UTC и передается в формате ISO 8601.
     */
    protected String captured_at;


    /*
    Время создания заказа. Указывается по UTC и передается в формате ISO 8601. Пример: 2017-11-03T11:52:31.827Z
     */
    protected String created_at;


    /*
    Время, до которого вы можете бесплатно отменить или подтвердить платеж.
    В указанное время платеж в статусе waiting_for_capture будет автоматически отменен.
    Указывается по UTC и передается в формате ISO 8601. Пример: 2017-11-03T11:52:31.827Z
     */
    protected String expires_at;

    /*
    Выбранный способ подтверждения платежа.
    Присутствует, когда платеж ожидает подтверждения от пользователя.
    Подробнее о сценариях подтверждения
     */
    protected Confirmation confirmation;

    /*
    Признак тестовой операции.
     */
    protected boolean test;

    /*
    Сумма, которая вернулась пользователю.
    Присутствует, если у этого платежа есть успешные возвраты.
     */
    protected Amount refunded_amount;

    /*
    Признак оплаты заказа.
     */
    protected boolean paid;


    /*
    Возможность провести возврат по API.
     */
    protected boolean refundable;

    /*
    Статус доставки данных для чека в онлайн-кассу (pending, succeeded или canceled).
     Присутствует, если вы используете решение Яндекс.Кассы для работы по 54-ФЗ .
     */
    protected String receipt_registration;


    /*
    Любые дополнительные данные, которые нужны вам для работы с платежами (например, номер заказа).
     Передаются в виде набора пар «ключ-значение» и возвращаются в ответе от Яндекс.Кассы.
     Ограничения: максимум 16 ключей, имя ключа не больше 32 символов, значение ключа не больше 512 символов.
     */
    protected Map<String, String> metadata;


    /*
    Комментарий к статусу canceled: кто отменил платеж и по какой причине.
     */
    protected CancellationDetails cancellation_details;

    /*
    Данные об авторизации платежа.
     */
    protected AuthorizationDetails authorization_details;

    /*
    Данные о распределении денег — сколько и в какой магазин нужно перевести.
    Присутствует, если вы используете решение Яндекс.Кассы для платформ .
     */
    protected List<Transfer> transfers;

    public Payment() {
        super();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public String getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(String payment_token) {
        this.payment_token = payment_token;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }

    public Amount getIncome_amount() {
        return income_amount;
    }

    public void setIncome_amount(Amount income_amount) {
        this.income_amount = income_amount;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public PaymentMethod getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(PaymentMethod payment_method) {
        this.payment_method = payment_method;
    }

    public String getCaptured_at() {
        return captured_at;
    }

    public void setCaptured_at(String captured_at) {
        this.captured_at = captured_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }

    public Confirmation getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Confirmation confirmation) {
        this.confirmation = confirmation;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Amount getRefunded_amount() {
        return refunded_amount;
    }

    public void setRefunded_amount(Amount refunded_amount) {
        this.refunded_amount = refunded_amount;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(boolean refundable) {
        this.refundable = refundable;
    }

    public String getReceipt_registration() {
        return receipt_registration;
    }

    public void setReceipt_registration(String receipt_registration) {
        this.receipt_registration = receipt_registration;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public CancellationDetails getCancellation_details() {
        return cancellation_details;
    }

    public void setCancellation_details(CancellationDetails cancellation_details) {
        this.cancellation_details = cancellation_details;
    }

    public AuthorizationDetails getAuthorization_details() {
        return authorization_details;
    }

    public void setAuthorization_details(AuthorizationDetails authorization_details) {
        this.authorization_details = authorization_details;
    }

    public List<Transfer> getTransfers() {
        return transfers;
    }

    public void setTransfers(List<Transfer> transfers) {
        this.transfers = transfers;
    }

    public Boolean getCapture() {
        return capture;
    }

    public void setCapture(Boolean capture) {
        this.capture = capture;
    }
}

