package ru.payda.backend.service.integration.ykassa.models;

public class  AuthorizationDetails{
    /*
    Retrieval Reference Number — уникальный идентификатор транзакции в системе эмитента.
    Используется при оплате банковской картой.
     */
    private String rrn;

    /*
    Код авторизации банковской карты. Выдается эмитентом и подтверждает проведение авторизации.
     */
    private String auth_code;

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getAuth_code() {
        return auth_code;
    }

    public void setAuth_code(String auth_code) {
        this.auth_code = auth_code;
    }
}
