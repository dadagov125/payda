package ru.payda.backend.service.integration.ykassa.models;

public class PaymentRequest extends Payment {

    private PaymentRequest(Amount amount, String payment_token, String description) {
        this.amount = amount;
        this.payment_token = payment_token;
        this.description = description;
    }

    public Amount getAmount() {
        return this.amount;
    }

    public String getPayment_token() {
        return this.payment_token;
    }

    public String getDescription() {
        return this.description;
    }

    public static class Builder {
        private Amount amount;

        private String payment_token;

        private String description;


        public Builder amount(Amount amount) {
            this.amount = amount;
            return this;
        }

        public Builder payment_token(String payment_token) {
            this.payment_token = payment_token;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public PaymentRequest build() {
            return new PaymentRequest(this.amount, this.payment_token, this.description);
        }
    }


}
