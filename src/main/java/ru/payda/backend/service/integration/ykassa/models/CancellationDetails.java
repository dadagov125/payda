package ru.payda.backend.service.integration.ykassa.models;

public class CancellationDetails{
    /*
    Участник процесса платежа, который принял решение об отмене транзакции.
    Может принимать значения yandex_checkout, payment_network и merchant.
     */
    private CancellationDetailsParty party;

    /*
    Причина отмены платежа. Перечень и описание возможных значений https://kassa.yandex.ru/developers/payments/declined-payments#cancellation-details-reason
     */
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public CancellationDetailsParty getParty() {
        return party;
    }

    public void setParty(CancellationDetailsParty party) {
        this.party = party;
    }
}
