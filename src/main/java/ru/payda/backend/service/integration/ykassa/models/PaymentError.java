package ru.payda.backend.service.integration.ykassa.models;

public class PaymentError {
    private String id;
    protected String type;
    protected String code;
    protected String description;
    protected String parameter;

    public PaymentError() {

    }

    public PaymentError(String id, String type, String code, String description, String parameter) {
        this.id = id;
        this.type = type;
        this.code = code;
        this.description = description;
        this.parameter = parameter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
