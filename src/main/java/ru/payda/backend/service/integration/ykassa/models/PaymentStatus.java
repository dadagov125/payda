package ru.payda.backend.service.integration.ykassa.models;

public enum PaymentStatus {
    init("init"),
    pending("pending"),
    waiting_for_capture("waiting_for_capture"),
    succeeded("succeeded"),
    canceled("canceled");


    private final String status;

    PaymentStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
