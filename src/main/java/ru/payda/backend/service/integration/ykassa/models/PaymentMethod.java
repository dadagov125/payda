package ru.payda.backend.service.integration.ykassa.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentMethod {
    /*
    Идентификатор способа оплаты.
     */
    private String id;

    /*
    Код способа оплаты.
     */
    private PaymentMethodType type;

    /*
    С помощью сохраненного способа оплаты можно проводить безакцептные списания .
     */
    private boolean saved;

    private  PaymentMethodCard card;

    /*
    Название способа оплаты.
     */
    private String title;

    /*
    Логин пользователя в Альфа-Клике (привязанный телефон или дополнительный логин).
     */
    private String login;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentMethodType getType() {
        return type;
    }

    public void setType(PaymentMethodType type) {
        this.type = type;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PaymentMethodCard getCard() {
        return card;
    }

    public void setCard(PaymentMethodCard card) {
        this.card = card;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
