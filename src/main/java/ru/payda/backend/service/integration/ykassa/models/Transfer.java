package ru.payda.backend.service.integration.ykassa.models;

public class Transfer{

    /*
    Идентификатор магазина, в пользу которого вы принимаете оплату.
    Выдается Яндекс.Кассой.
    Запросите идентификаторы нужных вам магазинов у вашего менеджера.
     */
    private String account_id;

    /*
    Сумма, которую необходимо перечислить магазину.
     */
    private Amount amount;

    /*
    Статус распределения денег между магазинами.
    Возможные значения: pending, waiting_for_capture, succeeded, canceled.
     */
    private PaymentStatus status;

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }
}
