package ru.payda.backend.service.integration.ykassa.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Amount {
    /*
    Сумма в выбранной валюте.
    Выражается в виде строки и пишется через точку, например 10.00.
    Количество знаков после точки зависит от выбранной валюты.
     */
    private String value;
    /*
    Код валюты в формате ISO-4217.
    Должен соответствовать валюте субаккаунта (recipient.gateway_id),
    если вы разделяете потоки платежей, и валюте аккаунта (shopId в личном кабинете),
    если не разделяете.
     */
    private String currency;

    public Amount() {

    }

    public Amount(String value, String currency) {
        this.value = value;
        this.currency = currency;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public static class Builder {
        private String value;
        private String currency;

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Amount build() {
            return new Amount(this.value, this.currency);
        }

    }
}
