package ru.payda.backend.service.integration.ykassa.models;

public enum CancellationDetailsParty{
    /*
    Продавец товаров и услуг (вы)
     */
    merchant,
    /*
    Яндекс.Касса
     */
    yandex_checkout,
    /*
    «Внешние» участники платежного процесса — все остальные участники платежного процесса,
    кроме Яндекс.Кассы и вас (например, эмитент, сторонний платежный сервис)
     */
    payment_network
}
