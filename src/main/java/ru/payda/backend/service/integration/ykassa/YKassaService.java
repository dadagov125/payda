package ru.payda.backend.service.integration.ykassa;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import ru.payda.backend.service.integration.ykassa.models.Payment;
import ru.payda.backend.service.integration.ykassa.models.PaymentError;
import ru.payda.backend.service.integration.ykassa.models.PaymentRequest;
import ru.payda.backend.service.integration.ykassa.models.PaymentResponse;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

// shopId 729274
// test_IxGi2kKbJC6Sbg3RJgiMtdqD1Fqm3P42QA7qK8tTEKU
@Service
public class YKassaService {
    private String apiUrl = "https://payment.yandex.net/api/v3/payments";


    @Autowired
    RestTemplate rest;

    public PaymentResponse createPayment(String idempotenceKey, PaymentRequest paymentRequest) throws Exception {
        PaymentResponse response = new PaymentResponse();
        ObjectMapper mapper = new ObjectMapper();
        try {


            String plainCreds = "729274:test_IxGi2kKbJC6Sbg3RJgiMtdqD1Fqm3P42QA7qK8tTEKU";
            byte[] plainCredsBytes = plainCreds.getBytes();
            byte[] base64CredsBytes = Base64.getEncoder().encode(plainCreds.getBytes(StandardCharsets.ISO_8859_1));
            String base64Creds = new String(base64CredsBytes);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + base64Creds);
            headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
            headers.add("Accept", MediaType.APPLICATION_JSON.toString());
            headers.add("Idempotence-Key", idempotenceKey);

            HttpEntity<PaymentRequest> requestEntity = new HttpEntity<>(paymentRequest, headers);

            ResponseEntity<String> responseEntity = rest.exchange(apiUrl, HttpMethod.POST, requestEntity, String.class);

            String json = responseEntity.getBody();

            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                response.setPayment(mapper.readValue(json, Payment.class));
            } else {
                response.setError(mapper.readValue(json, PaymentError.class));
            }
        } catch (HttpClientErrorException ex) {

            String json = ex.getResponseBodyAsString();
            response.setError(mapper.readValue(json, PaymentError.class));

        } catch (Exception ex) {
            response.setError(new PaymentError(null, null, null, ex.getMessage(), null));
        }

        return response;

    }

}
