package ru.payda.backend.service.integration.ykassa.models;

public class Recipient {

    /*
    Идентификатор магазина в Яндекс.Кассе.
     */
    private String account_id;


    /*
    Идентификатор субаккаунта.
    Используется для разделения потоков платежей в рамках одного аккаунта.
     */
    private String gateway_id;

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getGateway_id() {
        return gateway_id;
    }

    public void setGateway_id(String gateway_id) {
        this.gateway_id = gateway_id;
    }
}
