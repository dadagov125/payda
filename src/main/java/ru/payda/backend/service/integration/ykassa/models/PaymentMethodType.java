package ru.payda.backend.service.integration.ykassa.models;

public enum PaymentMethodType {
    bank_card,
    apple_pay,
    google_pay,
    //Электронные деньги
    yandex_money,
    qiwi,
    webmoney,
    wechat,
    //Интернет-банкинг
    sberbank,
    alfabank,
    tinkoff_bank,
    //B2B-платежи
    b2b_sberbank,
    //Другие способы
    mobile_balance,
    cash,
    installments
}
