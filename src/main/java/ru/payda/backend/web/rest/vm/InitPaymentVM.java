package ru.payda.backend.web.rest.vm;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

public class InitPaymentVM {
    @NotBlank
    private BigDecimal amount;

    @NotBlank
    private String currency;

    @NotBlank
    private String paymentToken;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentToken() {
        return paymentToken;
    }

    public void setPaymentToken(String paymentToken) {
        this.paymentToken = paymentToken;
    }
}
