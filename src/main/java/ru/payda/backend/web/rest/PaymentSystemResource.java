package ru.payda.backend.web.rest;

import ru.payda.backend.service.PaymentSystemService;
import ru.payda.backend.web.rest.errors.BadRequestAlertException;
import ru.payda.backend.service.dto.PaymentSystemDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link ru.payda.backend.domain.PaymentSystem}.
 */
@RestController
@RequestMapping("/api")
public class PaymentSystemResource {

    private final Logger log = LoggerFactory.getLogger(PaymentSystemResource.class);

    private static final String ENTITY_NAME = "paymentSystem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentSystemService paymentSystemService;

    public PaymentSystemResource(PaymentSystemService paymentSystemService) {
        this.paymentSystemService = paymentSystemService;
    }

    /**
     * {@code POST  /payment-systems} : Create a new paymentSystem.
     *
     * @param paymentSystemDTO the paymentSystemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new paymentSystemDTO, or with status {@code 400 (Bad Request)} if the paymentSystem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/payment-systems")
    public ResponseEntity<PaymentSystemDTO> createPaymentSystem(@Valid @RequestBody PaymentSystemDTO paymentSystemDTO) throws URISyntaxException {
        log.debug("REST request to save PaymentSystem : {}", paymentSystemDTO);
        if (paymentSystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new paymentSystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PaymentSystemDTO result = paymentSystemService.save(paymentSystemDTO);
        return ResponseEntity.created(new URI("/api/payment-systems/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /payment-systems} : Updates an existing paymentSystem.
     *
     * @param paymentSystemDTO the paymentSystemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated paymentSystemDTO,
     * or with status {@code 400 (Bad Request)} if the paymentSystemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the paymentSystemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/payment-systems")
    public ResponseEntity<PaymentSystemDTO> updatePaymentSystem(@Valid @RequestBody PaymentSystemDTO paymentSystemDTO) throws URISyntaxException {
        log.debug("REST request to update PaymentSystem : {}", paymentSystemDTO);
        if (paymentSystemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PaymentSystemDTO result = paymentSystemService.save(paymentSystemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, paymentSystemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /payment-systems} : get all the paymentSystems.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of paymentSystems in body.
     */
    @GetMapping("/payment-systems")
    public ResponseEntity<List<PaymentSystemDTO>> getAllPaymentSystems(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("fund-is-null".equals(filter)) {
            log.debug("REST request to get all PaymentSystems where fund is null");
            return new ResponseEntity<>(paymentSystemService.findAllWhereFundIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of PaymentSystems");
        Page<PaymentSystemDTO> page = paymentSystemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /payment-systems/:id} : get the "id" paymentSystem.
     *
     * @param id the id of the paymentSystemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the paymentSystemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/payment-systems/{id}")
    public ResponseEntity<PaymentSystemDTO> getPaymentSystem(@PathVariable Long id) {
        log.debug("REST request to get PaymentSystem : {}", id);
        Optional<PaymentSystemDTO> paymentSystemDTO = paymentSystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(paymentSystemDTO);
    }

    /**
     * {@code DELETE  /payment-systems/:id} : delete the "id" paymentSystem.
     *
     * @param id the id of the paymentSystemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/payment-systems/{id}")
    public ResponseEntity<Void> deletePaymentSystem(@PathVariable Long id) {
        log.debug("REST request to delete PaymentSystem : {}", id);

        paymentSystemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
