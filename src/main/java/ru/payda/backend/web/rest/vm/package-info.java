/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.payda.backend.web.rest.vm;
