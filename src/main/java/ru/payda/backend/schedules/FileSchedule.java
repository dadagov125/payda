package ru.payda.backend.schedules;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.payda.backend.repository.FileRepository;
import ru.payda.backend.service.FileStorageService;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileSchedule {

    private final FileRepository fileRepository;
    private final FileStorageService fileStorageService;

    public FileSchedule(FileRepository fileRepository, FileStorageService fileStorageService) {
        this.fileRepository = fileRepository;
        this.fileStorageService = fileStorageService;
    }

    //sec min houre dayOfMonh Month dayOfWeek  year
    @Scheduled(cron = "0 0 3 * * ?")
//    @Scheduled(fixedDelay = 5 * 1000) //for test
    void removeUnusedFiles() {

        List<String> filenames = fileStorageService.loadAll().map(Path::toString).collect(Collectors.toList());

        if (filenames.size() == 0)
            return;

        List<String> fileInfoNames = fileRepository.findAllNameInNames(filenames);

        List<String> unusedFiles = filenames.stream().filter(f -> !fileInfoNames.contains(f)).collect(Collectors.toList());

        unusedFiles.forEach(fileStorageService::delete);

    }
}
