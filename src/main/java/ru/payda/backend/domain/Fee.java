package ru.payda.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A Fee.
 */
@Entity
@Table(name = "fee")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Fee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "amount", precision = 21, scale = 2)
    private BigDecimal amount;

    @Column(name = "region")
    private String region;

    @NotNull
    @Column(name = "active", nullable = false)
    private Boolean active;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "fee_file",
        joinColumns = @JoinColumn(name = "fee_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "file_id", unique = true, referencedColumnName = "id"))
    private Set<FileInfo> files = new HashSet<>();

    @OneToMany(mappedBy = "fee")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Donation> donations = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = {"fees", "logo", "background"}, allowSetters = true)
    private Fund fund;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Fee name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Fee description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Fee amount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRegion() {
        return region;
    }

    public Fee region(String region) {
        this.region = region;
        return this;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Set<FileInfo> getFiles() {
        return files;
    }

    public void setFiles(Set<FileInfo> files) {
        this.files = files;
    }

    public Fee files(Set<FileInfo> files) {
        this.files = files;
        return this;
    }

    public Fee addFile(FileInfo file) {
        this.files.add(file);
        return this;
    }

    public Fee addFiles(List<FileInfo> files) {
        this.files.addAll(files);
        return this;
    }

    public Fee removeFile(FileInfo file) {
        this.files.remove(file);
        return this;
    }


    public Set<Donation> getDonations() {
        return donations;
    }

    public Fee donations(Set<Donation> donations) {
        this.donations = donations;
        return this;
    }

    public Fee addDonation(Donation donation) {
        this.donations.add(donation);
        donation.setFee(this);
        return this;
    }

    public Fee removeDonation(Donation donation) {
        this.donations.remove(donation);
        donation.setFee(null);
        return this;
    }

    public void setDonations(Set<Donation> donations) {
        this.donations = donations;
    }

    public Fund getFund() {
        return fund;
    }

    public Fee fund(Fund fund) {
        this.fund = fund;
        return this;
    }

    public void setFund(Fund fund) {
        this.fund = fund;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fee)) {
            return false;
        }
        return id != null && id.equals(((Fee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fee{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", amount=" + getAmount() +
            ", region='" + getRegion() + "'" +
            "}";
    }


    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
