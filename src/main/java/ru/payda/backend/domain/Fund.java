package ru.payda.backend.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Fund.
 */
@Entity
@Table(name = "fund")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Fund implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "region", nullable = false)
    private String region;


    //////

    @ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = {CascadeType.ALL})

    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinColumn(name = "logo")
    private FileInfo logo;


    @ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = {CascadeType.ALL})

    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinColumn(name = "background")
    private FileInfo background;


    @OneToOne
    @JoinColumn(unique = true)
    private PaymentSystem paymentSystem;

    @OneToMany(mappedBy = "fund")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Fee> fees = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "fund_user",
        joinColumns = @JoinColumn(name = "fund_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<User> users = new HashSet<>();


    public FileInfo getLogo() {
        return logo;
    }

    public void setLogo(FileInfo logo) {
        this.logo = logo;
    }

    public FileInfo getBackground() {
        return background;
    }

    public void setBackground(FileInfo background) {
        this.background = background;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Fund name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Fund description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegion() {
        return region;
    }

    public Fund region(String region) {
        this.region = region;
        return this;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public PaymentSystem getPaymentSystem() {
        return paymentSystem;
    }

    public Fund paymentSystem(PaymentSystem paymentSystem) {
        this.paymentSystem = paymentSystem;
        return this;
    }

    public void setPaymentSystem(PaymentSystem paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    public Set<Fee> getFees() {
        return fees;
    }

    public Fund fees(Set<Fee> fees) {
        this.fees = fees;
        return this;
    }

    public Fund addFees(Fee fee) {
        this.fees.add(fee);
        fee.setFund(this);
        return this;
    }

    public Fund removeFees(Fee fee) {
        this.fees.remove(fee);
        fee.setFund(null);
        return this;
    }

    public void setFees(Set<Fee> fees) {
        this.fees = fees;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Fund users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Fund addUser(User user) {
        this.users.add(user);
        return this;
    }

    public Fund removeUser(User user) {
        this.users.remove(user);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fund)) {
            return false;
        }
        return id != null && id.equals(((Fund) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Fund{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", region='" + getRegion() + "'" +
            "}";
    }


}
