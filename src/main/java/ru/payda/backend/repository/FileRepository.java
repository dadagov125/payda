package ru.payda.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.payda.backend.domain.FileInfo;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<FileInfo, Long> {
    @Query("SELECT f.name FROM FileInfo f WHERE f.name IN :names")
    List<String> findAllNameInNames(@Param("names") List<String> names);

}
