package ru.payda.backend.repository;

import ru.payda.backend.domain.Fund;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Fund entity.
 */
@Repository
public interface FundRepository extends JpaRepository<Fund, Long> {

    @Query(value = "select distinct fund from Fund fund left join fetch fund.users",
        countQuery = "select count(distinct fund) from Fund fund")
    Page<Fund> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct fund from Fund fund left join fetch fund.users")
    List<Fund> findAllWithEagerRelationships();

    @Query("select fund from Fund fund left join fetch fund.users where fund.id =:id")
    Optional<Fund> findOneWithEagerRelationships(@Param("id") Long id);
}
